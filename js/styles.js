var styles = [
  ['border:20px solid aquaMarine; width:100px; border-top:0px; border-bottom:0; height:320px; transform: rotate(-30deg); border-radius:100px;','border-right, border-left, border-radius'],

  ['border:0; border-right:5px solid yellow; border-bottom:5px solid yellow;  margin:20vh 0; width:100px; height:30vh; border-radius:0 70% 0 0;','border-right, border-bottom, border-left, height, border-radius'],

  ['width:10vw; border-radius: 2% 10% 50% 80%; border:20px inset hotpink; border-left:0; transform: rotate(8deg);','width, height, border-bottom, border-left, border-top'],

  ['border-with:0; width:20vw; border:34px dashed DarkViolet; border-radius:20px; transform: rotate(3deg); height: 25px;','border, height'],

  ['height:70vh; transform:rotate(-4deg); border:0; border-right:25px solid Gold; border-radius: 100%;','border-radius, border-right'],

  ['transform: perspective(500px) rotate3d(1,1,1,29deg); transform-style: preserve-3d; width:40vh;  border:20px solid black;','transform, border-left, border-bottom'],

  ['border: 10vw dotted RoyalBlue; border-radius: 0 0 100% 100%; width:80px; height:110px;','border, border-color'],

  ['border-top:0; border-left:5px solid Thistle; border-right:0; transform: perspective(68px) rotate3d(.1, 0, 1, 25deg); width:30vw; height:30vw; border-bottom: 140px solid Thistle; border-radius: 100px;','border-bottom, border-width'],

  ['border:10vw outset Wheat; transform: perspective(200px) rotate3d(1,1,1,10deg);','border, transform'],

  ['border:0; border-bottom:31vh outset transparent; border-left:31vh outset tomato; border-radius:60px;','border-bottom, border-left, border-radius'],

  ['border:5vw groove darkslateblue; transform: perspective(200px) rotate3d(1,1,1,-30deg);','border, transform'],

  ['border:2px solid #333; border-bottom:193px solid #333; border-left:23px double #333; width:34px; rotate3d(1,1,1,-30deg); height:77vh;','border-left, height'],

  ['border:0; border-top:20vw dotted #333; border-left:23vh outset YellowGreen; border-radius:60px;','border-top, border-left'],

]
