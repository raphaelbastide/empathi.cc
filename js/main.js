let d = document,
  b = d.body,
  main = d.querySelector('main'),
  draggable = d.querySelector('.draggable'),
  sub = d.querySelector('.subject'),
  influencers = d.querySelector('.influencers'),
  wait = idleTime = 0,
  limitDistance = 500,
  loopInterval = 300,
  autoPlayTime = 10000,
  blink = false,
  color = "lightgray",
  currentMode = 'helper', // interaction, auto-play, helper
  ua = /Android|webOS|iPhone|iPad|iPod|BlackBerry|IEMobile|Opera Mini/i.test(navigator.userAgent),
  link = document.querySelector("link[rel~='icon']"),
  audioReady =  ctx 

let totalStyles = styles.length
const synthArray = [
  {note:300, type: "triangle", dur: .04, gain: 0, baseGain: .6, env: "1,0"},
  {note:1200, type: "sine", dur: .03, gain: 0, baseGain: .7, env: "1,0"},
  {note:100, type: "square", dur: .05, gain: 0, baseGain: .17, env: "0,1,0"},
  {note:300, type: "noise", dur: .04, gain: 0, baseGain: .5, env: "0,1"},
  {note:1300, type: "onoise", dur: .03, gain: 0, baseGain: .3, env: "0,1,0"},
  {note:700, type: "triangle", dur: .05, gain: 0, baseGain: .4, env: "0,1,0"},
  {note:300, type: "sine", dur: .05, gain: 0, baseGain: .7, env: "1,0"},
  {note:800, type: "square", dur: .05, gain: 0, baseGain: .17, env: "0,1,0"},
  {note:780, type: "noise", dur: .04, gain: 0, baseGain: .5, env: "0,1"},
  {note:1500, type: "onoise", dur: .03, gain: 0, baseGain: .3, env: "0,1,0"},
  {note:900, type: "triangle", dur: .05, gain: 0, baseGain: .6, env: "1,0"},
  {note:200, type: "sine", dur: .05, gain: 0, baseGain: .7, env: "0,1,0"},
  {note:120, type: "onoise", dur: .04, gain: 0, baseGain: .3, env: "1,0"},
  {note:1100, type: "square", dur: .05, gain: 0, baseGain: .17, env: "0,1,0"},
  {note:90, type: "noise", dur: .04, gain: 0, baseGain: .5, env: "0,1,0"},
  {note:300, type: "onoise", dur: .04, gain: 0, baseGain: .3, env: "0,1,0"},
]
let synth = synthArray[0]
let dragActive = false;
let baseNote = 450;
let note = limitDistance;

(function init(){
  document.addEventListener("touchstart", touchHandler, true);
  document.addEventListener("touchmove", touchHandler, true);
  document.addEventListener("touchend", touchHandler, true);
  document.addEventListener("touchcancel", touchHandler, true);

  step()
  draggable.addEventListener('click', function() {
    checkAudioCtx()
  });

  // Looping
  function step() {
    setTimeout(function () {
      if(synth.gain != 0) playNote(synth.note, synth, 0)
      if(blink) blinkObject()
      step();
    }, loopInterval);
  };

  // Create graphic objects
  for (let v = 0; v < totalStyles; v++) {
    let r = Math.floor(Math.random() * styles.length)
    let rs = styles[r]
    let index = styles.indexOf(rs);
    createObject(rs, index)
    if (index > -1) {
      styles.splice(index, 1);
    }
  }
  // First favicon update
  updateFavicon(color);
  // Center the div at the beginning
  draggable.style.left = "calc(50vw - 60px)";
  draggable.style.opacity = "1";
  draggable.style.top = "10vh";
  // Make the DIV element draggable:
  dragElement(draggable);
  // Select influencers after they are created
  influencer = d.querySelectorAll('.influencer')
  // Generates all synths and push them into an array. Each synth matches with one inf
  influencer.forEach((item, i) => {
    // let currentSynth = generateRandomSynth();
    // synthArray.push(currentSynth);
  });

  // Scroll event
  window.addEventListener('wheel', throttle(scrollEvt, 8, 1000));
  // Check state every 1s
  setInterval(function() {
    updateStyle()
    // if (audioReady) {
      updateSound()
    // }
  }, 100);

  // Helper mode
  changeMode('helper')
  // Autoplay mode check
  setInterval(function() {
    autoplaySequence()
  }, autoPlayTime);
})()

function autoplaySequence() {
  if (currentMode !== 'auto-play' && dragActive == false) {
    if (idleTime >= 2) {
      changeMode('auto-play')
    }
    idleTime++
  } else if (currentMode == 'auto-play') {
    // autoplay move animation
    setTimeout(function() {
      let rx = getRandomInt(10, 90);
      let ry =  getRandomInt(10, 90);
      draggable.style.left = rx + "vw"
      draggable.style.top = ry + "vh"
    }, 10);
  }
}

function updateStyle(waitLimit = 15) {
  let subCenter = getCenter(sub)
  for (let i = 0; i < influencer.length; i++) {
    let influencerCenter = getCenter(influencer[i])
    let distance = getDistance(subCenter, influencerCenter)
    if (distance <= limitDistance) { //close enough
      mergeStyles(sub, influencer[i])
      // changeMode('interaction')
      wait = 0
    } else {
      if (wait >= waitLimit) {
        setTimeout(function() {
          sub.style = "" // clear all inline styles
          color = "gray"
          b.style.backgroundColor = color
          updateFavicon(color)
        }, 11);
      }
      wait++
    }
  }
}

function getCenter(item) {
  let pos = item.getBoundingClientRect()
  let center = [pos.x + pos.width / 2, pos.y + pos.height / 2]
  return {
    x: center[0],
    y: center[1]
  }
}

function getDistance(a, b) {
  return Math.hypot(a.x - b.x, a.y - b.y);
}

function mergeStyles(subject, influencer) {
  let styleList = influencer.getAttribute('data-share')
  styleList = styleList.split(', ')
  setTimeout(function() {
    for (let i = 0; i < styleList.length; i++) {
      let styleToMerge = styleList[i]
      let prop = influencer.style[styleToMerge]
      subject.style[styleToMerge] = prop
      let props = prop.split(' ')
      for (let k = 0; k < props.length; k++) {
        if (chroma.valid(props[k])) {
          // color = chroma(props[k]).luminance(0.7).desaturate(.1)
          color = chroma(props[k]).set('hsl.h', 0).brighten(1)
          b.style.backgroundColor = color
          updateFavicon(color)
        }
      }
    }
  }, 100); // avoid style flickering
}

// Drag
function dragElement(elmnt) {
  let pos1 = 0,
    pos2 = 0,
    pos3 = 0,
    pos4 = 0;
  if (document.getElementById(elmnt.id + "header")) {
    // if present, the header is where you move the DIV from:
    document.getElementById(elmnt.id + "header").onmousedown = dragMouseDown;
  } else {
    // otherwise, move the DIV from anywhere inside the DIV:
    elmnt.onmousedown = dragMouseDown;
  }

  function dragMouseDown(e) {
    e = e || window.event;
    e.preventDefault();
    // if (currentMode == "auto-play") {
      changeMode('interaction')
    // }
    // get the mouse cursor position at startup:
    pos3 = e.clientX;
    pos4 = e.clientY;
    document.onmouseup = closeDragElement;
    // call a function whenever the cursor moves:
    document.onmousemove = elementDrag;
  }

  function elementDrag(e) {
    dragActive = true;
    e = e || window.event;
    e.preventDefault();
    // calculate the new cursor position:
    pos1 = pos3 - e.clientX;
    pos2 = pos4 - e.clientY;
    pos3 = e.clientX;
    pos4 = e.clientY;
    // set the element's new position:
    elmnt.style.top = (elmnt.offsetTop - pos2) + "px";
    elmnt.style.left = (elmnt.offsetLeft - pos1) + "px";
  }

  function closeDragElement() {
    // stop moving when mouse button is released:
    dragActive = false;
    document.onmouseup = null;
    document.onmousemove = null;
  }
}

function changeMode(modeName) {
  b.setAttribute('class', '')
  b.classList.add(modeName)
  currentMode = modeName
  console.log('mode: ' + modeName);
  if (modeName == "auto-play") {
    d.title = "empathi.cc  ∞ ▸"
    autoplaySequence()
    pageScroll()
  } else {
    idleTime = 0
    d.title = "empathi.cc"
  }
}

// favicon
function updateFavicon(color) {
  let canvas = document.createElement("canvas");
  canvas.width = 16;
  canvas.height = 16;
  let context = canvas.getContext("2d");
  context.fillStyle = color;
  context.fillRect(0, 0, 16, 16);
  context.fill();
  link.type = "image/x-icon";
  link.href = canvas.toDataURL();
};

function throttle(fn, attr, wait) {
  let time = Date.now();
  return function() {
    if ((time + wait - Date.now()) < 0) {
      fn(attr);
      time = Date.now();
    }
  }
}

function pageScroll() {
  window.scrollBy(0, 1);
  if (document.documentElement.scrollTop >= b.offsetHeight - 1000) {
    // window.location.reload(true);
    window.location.href = window.location.href
  }
  if (currentMode == "auto-play") {
    scrolldelay = setTimeout(pageScroll, 100);
  }
}

function scrollEvt() {
  if (currentMode !== 'interaction') {
    changeMode('interaction')
  }
  updateSound();
}

function createObject(styles, index) {
  let o = d.createElement('div')
  let rleft = Math.floor(Math.random() * 80) - 40
  let styleLine = styles[0]
  let share = styles[1]
  o.classList.add('object')
  o.classList.add('influencer')
  o.setAttribute('style', styleLine)
  o.style.left = rleft + "vw"
  o.setAttribute('data-share', share)
  o.setAttribute('data-id', index)
  influencers.appendChild(o)
}

function updateSound(waitLimit = 15) {
  let subCenter = getCenter(sub)
  for (let i = 0; i < influencer.length; i++) {
    let influencerCenter = getCenter(influencer[i])
    let distance = getDistance(subCenter, influencerCenter)
    if (distance <= limitDistance) { //close enough
      // clone object
      synth = {...synthArray[i]}
      loopInterval = distance
      // change note depending on the distance
      synth.note = synth.note + (Math.round(distance) / 4)
      console.log(Math.round(distance) / 5, synth.note);
      if (synth.note < 0) synth.note = 0;
      if (loopInterval <= 60) {
        loopInterval = 60
      }
      synth.gain = synthArray[i].baseGain
      wait = 0
      blink = true
    }else{
      if (wait >= waitLimit) {
        synth.gain = 0
        blink = false
      }
      wait++
    }
  }
}

function getRandomInt(min, max) {
  min = Math.ceil(min);
  max = Math.floor(max);
  return Math.floor(Math.random() * (max - min) + min); //The maximum is exclusive and the minimum is inclusive
}

function getRandomArbitrary(min, max) {
  return Math.random() * (max - min) + min;
}

function touchHandler(event) {
  let touch = event.changedTouches[0];
  let simulatedEvent = document.createEvent("MouseEvent");
    simulatedEvent.initMouseEvent({
    touchstart: "mousedown",
    touchmove: "mousemove",
    touchend: "mouseup"
  }[event.type], true, true, window, 1,
    touch.screenX, touch.screenY,
    touch.clientX, touch.clientY, false,
    false, false, false, 0, null);
  touch.target.dispatchEvent(simulatedEvent);
  event.preventDefault();
}

function blinkObject(){
  draggable.style.transform = `scale(1.02)`
  setTimeout(() => {
    draggable.style.transform = "none"
  }, 50);
}