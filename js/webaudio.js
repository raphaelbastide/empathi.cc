// Audio Context
let ctx
window.addEventListener("mousedown", checkAudioCtx);
window.addEventListener("keydown", checkAudioCtx);
function checkAudioCtx(){
  if(ctx == undefined || ctx.state == "closed" || ctx.state == "suspended"){
    ctx = new(window.AudioContext || window.webkitAudioContext)();
    console.log("context created");
  }
}

let hasReverb

function playNote(note, synth, panVal=0) {
  hasReverb = false
  if (ctx == undefined) {
    return
  }
  // do not play if gain or note = 0:
  if(synth.gain != 0 && note != "0" && note != 0){
    // Main osc
    let osc

    // Parse floats so minor typo don’t fatal error
    synth.dur = parseFloat(synth.dur)
    synth.gain = parseFloat(synth.gain)

    // handles multisynths types (synth type names separated by underscores)
    if (synth.type.includes("_")) {
      multiSynth = synth.type.split("_")
      multiSynth.forEach(synthType => {
        synth.type = synthType
        playNote(note, synth, panVal)
      })
      return
    }
    // same for notes. Things need to be dirty sometimes for life to be fun
    if (note.toString().includes("_")) {
      multiSynth = note.toString().split("_")
      // no multisynth for noise, as it clogs the gain
      if (synth.type == "noise") {
        playNote(multiSynth[0], synth, panVal)        
      }else{
        multiSynth.forEach(synthNote => {
          note = synthNote
          playNote(note, synth, panVal)
        })
      }
      return
    }else{
      // recheck if converted note == 0. Avoid a bug with buffers (onoise, noise)
      if(note == "0" || note == 0) return
    }

    if(synth.type == 'noise'){
      synth.gain = synth.gain
      osc = whiteNoise(ctx, synth.gain, synth.dur)
    }else if(synth.type == 'onoise'){
      osc = noiseOscillator(ctx, note, synth.dur, 1, 1, 1, 1)
    }else{
      osc = ctx.createOscillator()
      osc.frequency.setValueAtTime(note, ctx.currentTime);
      osc.type = synth.type
    }

    // Main gain
    const oscGain = ctx.createGain()
    oscGain.gain.value = synth.gain 
    // changing envelope string to array, doing that in solve() creates a mess
    env = synth.env.split(" ").join("").split(',')
    // map() relates gain vals to oscGain
    oscGain.gain.setValueCurveAtTime(new Float32Array(env).map(function(x){return x * synth.gain}), ctx.currentTime, synth.dur); 
  

    if (hasReverb) {
      // Reverb
      const reverbBuffer = algoReverb(.4, .1, false)
      const reverb = ctx.createConvolver()
      reverb.buffer = reverbBuffer
      const wetGain = ctx.createGain()
      wetGain.gain.value = 1
      // oscGain -> wetGain -> reverb -> destination
      oscGain.connect(wetGain)
      wetGain.connect(reverb)
      reverb.connect(ctx.destination)
    }


    // Panner
    let panNode = ctx.createStereoPanner()
    panNode.pan.value = panVal
    osc.connect(panNode)
    // panner depends on the main gain
    panNode.connect(oscGain) 

    // Oscillator
    // oscGain comes from panNode
    oscGain.connect(ctx.destination)

    osc.start(ctx.currentTime)
    osc.stop(ctx.currentTime + synth.dur)
  }
}

function algoReverb(seconds, decay, reverse){
  let rate = ctx.sampleRate
  let length = rate * seconds
  // createBuffer is deprecated, change it to decodeAudioData()
  let impulse = ctx.createBuffer(1, length, rate)
  let impulseC = impulse.getChannelData(0)
  let n, i
  for (i = 0; i < length; i++) {
    n = (reverse) ? length-i : i
    impulseC[i] = (Math.random()*2 - 1) * Math.pow( 1 - n/length, decay)
  }
  return impulse
}

// White noise
function whiteNoise(ctx, ampl, dur){
  // 10 is arbirtrary time in sec for the buffer to last, long enough to cover most cases, will be overwriten by beat duration anyway.
  const whiteBuffer = ctx.createBuffer(2, ctx.sampleRate*dur, ctx.sampleRate)
  for (let ch=0; ch<whiteBuffer.numberOfChannels; ch++) {
    let samples = whiteBuffer.getChannelData(ch)
    for (let s=0; s<whiteBuffer.length; s++) samples[s] = Math.random()* ampl * .5- ampl
  }
  return new AudioBufferSourceNode(ctx, {buffer:whiteBuffer})
}

// onoise
function noiseOscillator(ctx, note, dur, res, noise, flat, dist){
  // thx adl!
  let pattern = [];
  let i = 0;
  while (i < ctx.sampleRate/note) {
   let rdm_val = Math.random() - Math.random();
    let val = (Math.sin(2*Math.PI*note * (i/ctx.sampleRate)) + rdm_val * noise) * dist;
    val = Math.floor(val * res)/res;
    for (var r = 0; r < flat; r++) {
      pattern.push(val > 1 ? 1 : val < -1 ? -1 : val);
      i++;
    }
  }
  const buffer = ctx.createBuffer(1, ctx.sampleRate*dur, ctx.sampleRate)
  for (let ch=0; ch<buffer.numberOfChannels; ch++) {
    let samples = buffer.getChannelData(ch);
    for (let s=0; s<buffer.length; s++) {
      samples[s] = pattern[s > pattern.length-1 ? s - Math.floor(s/pattern.length) * pattern.length : s ];
    }
  }
  return new AudioBufferSourceNode(ctx, {buffer:buffer})
}

function clamp(val, min, max) {
  return val > max ? max : val < min ? min : val;
}

