# empathi.cc

Raphaël Bastide 2020

The ability to understand and share other people’s feelings and problems is known as empathy. On the web page empathi.cc, artist Raphaël Bastide created a graphic element which can be moved and can mimic visual attributes from its neighbors. The style transfer between the page’s elements is considered as a technical equivalent of empathy, where the distance between objects triggers visual mutations. Consequently, the main graphic element gets affected by an accumulating memory of style properties depending on its path.

Technique: HTML, CSS, Javascript  
Assisted by [Alice Ricci](https://alicericci.eu/)

Updated in 2024: webaudio optimisation and animations